/**
 * Created by reiner on 09.06.16.
 */
angular.module('app.test').controller("testController", function($scope, $location){

    $scope.main = {};

    $scope.main.newItem;

    $scope.main.items=['Item1', 'Item2', 'Item3'];

    $scope.main.addItem = function(itemToAdd){
        $scope.main.items.push(itemToAdd);
        $scope.main.newItem = '';
    }

    $scope.main.showMainPage = function(){
        $location.path('/welcome');
    }

})