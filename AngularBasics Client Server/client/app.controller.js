/**
 * Created by reiner on 09.06.16.
 */
angular.module('app').controller("MainController", function($scope, $location){

    $scope.main = {};

    $scope.main.title = 'AngularJS Tutorial Day4Solutions';
    $scope.main.searchInput = '';


    $scope.main.showItemsPage = function(){
        $location.path('/items');
    }

});