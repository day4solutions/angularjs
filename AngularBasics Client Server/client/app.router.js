/**
 * Created by reiner on 09.06.16.
 */
angular.module('app').config(function ($routeProvider) {
        $routeProvider
            .when('/', {templateUrl: 'welcome.html', controller: 'MainController'})
            .when('/items', {templateUrl: 'items.html', controller: 'testController'})
            .otherwise({redirectTo: '/'});
    })